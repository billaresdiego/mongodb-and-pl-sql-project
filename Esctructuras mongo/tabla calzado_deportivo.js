db.createCollection("calzado_deportivo",
{
    validator: {
        $jsonSchema: {
            bsonType: "object",
            required: [
                "marca",
                "modelo",
                "recibir_notificacion"
            ],
            properties: {
                marca: {
                    bsonType: "string",
                    description: "Debe ser un string y es requerido"
                },
                modelo: {
                    bsonType: "string",
                    description: "Debe ser un string y es requerido"
                },
                apodo: {
                    bsonType: "string",
                    description: "Debe ser un string"
                },
                notas_adicionales: {
                    bsonType: "string",
                    description: "Debe ser un string"
                },
                recibir_notificacion: {
                    bsonType: "bool",
                    description: "Debe ser un booleano y es requerido"
                }
            }
        }
    },
});
