db.createCollection("datos_basicos",
{
    validator: {
        $jsonSchema: {
            bsonType: "object",
            required: [
                "email",
                "nombre",
                "apellido",
                "fecha_de_nacimiento"
            ],
            properties: {
                email: {
                    bsonType: "string",
                    description: "El mail debe ser un string y es requerido"
                },
                nombre: {
                    bsonType: "string",
                    description: "El nombre debe ser un string y es requerido"
                },
                genero: {
                    enum: [
                        "Masculino",
                        "Femenino",
                        "Otro"
                    ],
                    description: "Solo puede ser uno de los valores del enum"
                },
                fecha_de_nacimiento: {
                    bsonType: "date",
                    description: "Debe ser una fecha y es requerido"
                },
               ubicacion: {
                    bsonType: "object",
                    required: [
                        "calle",
                        "numero"
                    ],
                    properties: {
                        calle: {
                            bsonType: "string",
                            description: "La calle debe ser un string",
                        },
                        numero: {
                            bsonType: "int",
                            description: "El numer de puerta debe ser un entero",
                        },
                    },
                },
                altura: {
                    bsonType: "double",
                    description: "La altura debe ser un numero"
                },
                peso: {
                    bsonType: "double",
                    description: "El peso debe ser un numero"
                },
                biografia: {
                    bsonType: "string",
                    description: "La biografia debe ser un string"
                },
                url: {
                    bsonType: "string",
                    description: "La URL debe ser un string"
                },
                preferencias: {
                    bsonType: "object",
                    required: [
                        "unidades_y_medidas",
                        "temperatura",
                        "deporte"
                    ],
                    properties: {
                        unidades_y_medidas: {
                        enum: [
                                "km y kg",
                                "mi, lb"
                            ],
                        description: "Solo puede ser uno de los valores del enum"
                        },
                        temperatura: {
                            enum: [
                                "f",
                                "c"
                            ],
                            description: "Solo puede ser uno de los valores del enum"
                        },
                        deporte: {
                            enum: [
                                "carrera",
                                "ciclismo"
                            ],
                            description: "Solo puede ser uno de los valores del enum"
                        }
                    }
                },
                controles_de_privacidad: {
                    bsonType: "object",
                    required: [
                        "pagina_de_perfil",
                        "actividades_individuales",
                        "actividades_en_grupo"
                    ],
                    properties: {
                        pagina_de_perfil: {
                            enum: [
                                "todos",
                                "seguidores"
                            ],
                            description: "Solo puede ser uno de los valores del enum"
                        },
                        actividades_individuales: {
                            enum: [
                                "todos",
                                "seguidores"
                            ],
                            description: "Solo puede ser uno de los valores del enum"
                        },
                        actividades_en_grupo: {
                            enum: [
                                "todos",
                                "seguidores"
                            ],
                            description: "Solo puede ser uno de los valores del enum"
                        }
                    }
                },
                equipacion_bicicletas: {
                    bsonType: "array",
                    description: "Se requiere un array con las ID de las bicicletas asociadas"
                },
                equipacion_calzado: {
                    bsonType: "array",
                    description: "Se requiere un array con las ID de los calzados asociados"
                }
            }
        }
    }
}
);