db.createCollection("bicicleta",
{
    validator: {
        $jsonSchema: {
            bsonType: "object",
            required: [
                "nombre",
                "tipo",
                "marca",
                "modelo"
            ],
            properties: {
                nombre: {
                    bsonType: "string",
                    description: "Debe ser un string y es requerido"
                },
                tipo: {
                    bsonType: "string",
                    description: "Debe ser un string y es requerido"
                },
                peso: {
                    bsonType: "double",
                    description: "Debe ser un numero"
                },
                modelo: {
                    bsonType: "string",
                    description: "Debe ser un string y es requerido"
                },
                marca: {
                    bsonType: "string",
                    description: "Debe ser un string y es requerido"
                },
                notas_adicionales: {
                    bsonType: "string",
                    description: "Debe ser un string"
                }
            }
        }
    },
});
