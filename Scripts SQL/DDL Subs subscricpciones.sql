--SCRIPT CREACION TABLAS DDL

CREATE TABLE PLAN (
nombre varchar(45) primary key, 
fechaCreacion date not null,
fechaActivacion date not null,
fechaDesactivacion date default null,
porcentajeDescuento number(10, 0) default 0 not null,
tarifaMensual long not null,
activado number(1) default 1 not null CHECK(activado IN (0,1)),
CHECK((porcentajeDescuento >= 0) AND (porcentajeDescuento <= 100))
);


CREATE TABLE Usuario (
usuario varchar(45) primary key,
nombre varchar(45) not null,
apellidos varchar(45) not null,
fechaNacimiento date not null,
correoElectronico varchar(45) not null,
clave varchar(45) not null
);



CREATE TABLE Subscripcion(
idSubscripcion integer primary key,
usuario varchar(45) references Usuario(usuario) not null,
nombrePlan varchar(45) references Plan(nombre) not null,
fechaInicio date not null,
fechaCancelacion date default null,
metodoPago varchar(45) default null CHECK(metodoPago IN ('tarjeta de credito', 'PayPal')),
tipoPago varchar(45) default 'mensual' not null CHECK(tipoPago IN ('mensual', 'anual'))
);


CREATE TABLE Factura(
codigo integer primary key,
idSubscripcion integer references Subscripcion(idSubscripcion),
montoAPagar number(10,0) not null,
fechaEfectuado timestamp,
fechaCreada timestamp not null,
estaPago number(1) default 0 not null CHECK (estaPago IN (0, 1))
);

