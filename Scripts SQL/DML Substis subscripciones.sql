--SCRIPT INSERCION DATOS DML

INSERT INTO usuario 
(usuario, nombre, apellidos, fechanacimiento, correoelectronico, clave)
VALUES
('RobertoSan','Roberto', 'Sanchez', TO_DATE('17/05/1991', 'DD/MM/YYYY'), 'roberto@test.com', '1234');

INSERT INTO usuario 
(usuario, nombre, apellidos, fechanacimiento, correoelectronico, clave)
VALUES
('MarianoLenos','Mariano', 'Lenos', TO_DATE('17/03/1994', 'DD/MM/YYYY'), 'mariano@test.com', '5678');

INSERT INTO plan 
(nombre, fechacreacion, fechaactivacion, fechadesactivacion, porcentajedescuento, tarifamensual, activado)
VALUES
('Antel', TO_DATE('15/01/2001', 'DD/MM/YYYY'), TO_DATE('15/03/2001', 'DD/MM/YYYY'), null, 25, 1200, 1);

INSERT INTO subscripcion
(idsubscripcion, usuario, nombreplan, fechainicio, fechacancelacion, metodopago, tipopago)
VALUES
(1,'RobertoSan', 'Antel', TO_DATE('22/03/2009', 'DD/MM/YYYY'), null, 'PayPal', 'mensual');

INSERT INTO usuario 
(usuario, nombre, apellidos, fechanacimiento, correoelectronico, clave)
VALUES
('LeandroInalu','Leandro', 'Alunialu', TO_DATE('17-04-1990', 'DD/MM/YYYY'), 'leandro@email.com', '987');

INSERT INTO usuario 
(usuario, nombre, apellidos, fechanacimiento, correoelectronico, clave)
VALUES
('FacundoMinosi','Facundo', 'Miarisioni', TO_DATE('17/04/1999', 'DD/MM/YYYY'), 'facundo@gmail.com', '69847');

INSERT INTO subscripcion
(idsubscripcion, usuario, nombreplan, fechainicio, fechacancelacion, metodopago, tipopago)
VALUES
(4,'RobertoSan', 'Antel', TO_DATE('22/01/2021', 'DD/MM/YYYY'), null, 'PayPal', 'mensual');

INSERT INTO subscripcion
(idsubscripcion, usuario, nombreplan, fechainicio, fechacancelacion, metodopago, tipopago)
VALUES
(5,'RobertoSan', 'Antel', TO_DATE('22/01/2020', 'DD/MM/YYYY'), null, 'PayPal', 'anual');

INSERT INTO subscripcion (idsubscripcion, usuario, nombreplan, fechainicio, fechacancelacion, metodopago, tipopago)
VALUES (7,'LeandroInalu', 'Antel', TO_DATE('17/08/2008', 'DD/MM/YYYY'), null, 'tarjeta de credito', 'mensual');

INSERT INTO subscripcion (idsubscripcion, usuario, nombreplan, fechainicio, fechacancelacion, metodopago, tipopago)
VALUES (8,'FacundoMinosi', 'Antel', TO_DATE('22/06/2020', 'DD/MM/YYYY'), null, 'PayPal', 'anual');

INSERT INTO plan 
(nombre, fechacreacion, fechaactivacion, fechadesactivacion, porcentajedescuento, tarifamensual, activado)
VALUES
('Movistar',TO_DATE('16/01/1997', 'DD/MM/YYYY'), TO_DATE('05/03/2000', 'DD/MM/YYYY'), null, 12, 475, 1);

INSERT INTO subscripcion
(idsubscripcion, usuario, nombreplan, fechainicio, fechacancelacion, metodopago, tipopago)
VALUES
(9,'LeandroInalu', 'Movistar', TO_DATE('23/01/2003', 'DD/MM/YYYY'), null, 'PayPal', 'anual');
