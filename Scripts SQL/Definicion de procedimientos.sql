--====================================================
--Cancelar subscripcion.
--====================================================

CREATE OR REPLACE FUNCTION GENERAR_ID_SUBSCRIPCION RETURN INTEGER AS
    
    cSubscripciones INTEGER;
    idSubscripcion INTEGER;
BEGIN 
    SELECT COUNT(1) INTO cSubscripciones
    FROM subscripcion;
    
    IF cSubscripciones = 0 THEN
        commit;
        return 1;
    ELSE 
        SELECT MAX(idSubscripcion) INTO idSubscripcion
        FROM subscripcion;
        commit;
        return  idSubscripcion + 1;
    END IF;
EXCEPTION 
    WHEN OTHERS THEN 
       RAISE_APPLICATION_ERROR(-20003, 'Se produjo un error el cual no fue necesario rollback'); 
END;

CREATE OR REPLACE PROCEDURE CANCELAR_SUBSCRIPCION (usuario_subscripcion IN usuario.usuario%TYPE) AS 
    CURSOR cur_subscripciones is 
    SELECT *
    FROM subscripcion   
    WHERE usuario = usuario_subscripcion;
    
    vSubscripcion subscripcion%ROWTYPE;
    cUsuarios NUMBER := 0;
    idSubscripcionInsertar NUMBER;
BEGIN
    SELECT COUNT(1) INTO cUsuarios
    FROM subscripcion s  
    WHERE s.usuario = usuario_subscripcion;
    
    IF (cUsuarios = 0) THEN
        RAISE_APPLICATION_ERROR(-20001, 'No existe el usuario'); 
        commit;
    END IF;
    
    OPEN cur_subscripciones; 
	LOOP
        FETCH cur_subscripciones INTO vSubscripcion;
		EXIT WHEN cur_subscripciones%NOTFOUND;
        BEGIN
        
        IF vSubscripcion.fechacancelacion is null THEN
            UPDATE 
                subscripcion
            SET
                subscripcion.fechacancelacion = sysdate
            WHERE 
                subscripcion.usuario = vSubscripcion.usuario;
        commit;
            idSubscripcionInsertar := GENERAR_ID_SUBSCRIPCION;
            INSERT INTO subscripcion
            (idSubscripcion, usuario, nombrePlan, fechaInicio, fechaCancelacion, metodoPago, tipoPago)
            VALUES
            (idSubscripcionInsertar, vSubscripcion.usuario, 'Prueba', sysdate, null, vSubscripcion.metodopago, vSubscripcion.tipopago); 
        commit;
        END IF;
        EXCEPTION 
            WHEN OTHERS THEN 
                ROLLBACK;	
	    commit;
	    RAISE_APPLICATION_ERROR(-20004, 'Se produjo un error el cual fue necesario rollback'); 
        END;
    END LOOP;
	CLOSE cur_subscripciones;
END;

--====================================================
--Eliminar una cuenta
--====================================================

CREATE OR REPLACE PROCEDURE BORRAR_SUBSCRIPCIONES (usuario_eliminar IN usuario.usuario%TYPE) AS 
    CURSOR cur_subscripciones is 
    select *
    from subscripcion
    where usuario = usuario_eliminar;
    
    vSubscripcion subscripcion%ROWTYPE;
BEGIN
    OPEN cur_subscripciones; 
        LOOP
        FETCH cur_subscripciones INTO vSubscripcion;
            EXIT WHEN cur_subscripciones%NOTFOUND;
        BEGIN 
            DELETE 
            FROM 
                subscripcion
            WHERE
                subscripcion.usuario = usuario_eliminar;
        commit;
        EXCEPTION 
            WHEN OTHERS THEN
                ROLLBACK;
	    commit;
	    RAISE_APPLICATION_ERROR(-20004, 'Se produjo un error el cual fue necesario rollback'); 
        END;
        END LOOP;
	CLOSE cur_subscripciones;
END;

CREATE OR REPLACE PROCEDURE PONER_NULLS_FACTURA(usuario_eliminar IN usuario.usuario%TYPE) AS
      CURSOR cur_factura IS 
      SELECT subscripcion.idsubscripcion
      FROM factura, subscripcion
      where factura.idsubscripcion = subscripcion.idsubscripcion 
      and subscripcion.usuario = usuario_eliminar;
      
BEGIN
    FOR vFactura IN cur_factura
        LOOP
            BEGIN
                UPDATE 
                    factura
                SET 
                    idsubscripcion = null
                WHERE 
                    idsubscripcion = vFactura.idsubscripcion;
            commit;
            EXCEPTION 
            WHEN OTHERS THEN
                ROLLBACK;
	    commit;
	    RAISE_APPLICATION_ERROR(-20004, 'Se produjo un error el cual fue necesario rollback'); 
            END;
    END LOOP;
END;

CREATE OR REPLACE PROCEDURE ELIMINAR_CUENTA (usuario_eliminar IN usuario.usuario%TYPE) AS 
    cUsuarios NUMBER := 0;
BEGIN
    SELECT COUNT(1) INTO cUsuarios
    FROM subscripcion s  
    WHERE s.usuario = usuario_eliminar;
    
    IF (cUsuarios = 0) THEN
        RAISE_APPLICATION_ERROR(-20001, 'No existe el usuario'); 
        commit;
    END IF;
    
  
    PONER_NULLS_FACTURA(usuario_eliminar);
    BORRAR_SUBSCRIPCIONES(usuario_eliminar);
    
    
    DELETE 
    FROM 
        usuario
    WHERE
        usuario.usuario = usuario_eliminar;
    commit;
EXCEPTION 
    WHEN OTHERS THEN
        ROLLBACK;
        commit;
        RAISE_APPLICATION_ERROR(-20004, 'Se produjo un error el cual fue necesario rollback'); 
END;


--====================================================
--Historial de suscripciones y pagos
--====================================================

CREATE OR REPLACE FUNCTION HISTORIAL_CON_PAGOS (usuario_subscripcion usuario.usuario%TYPE) RETURN VARCHAR2 AS 
    CURSOR cur_sub_pagos IS
    SELECT nombreplan, fechainicio, fechacancelacion, tipopago, metodopago, fechaefectuado, montoapagar 
    FROM subscripcion, factura
    WHERE subscripcion.usuario = usuario_subscripcion
    and subscripcion.idsubscripcion = factura.idsubscripcion;
    
    
    sHistorial VARCHAR2(30000) := '';
BEGIN 
    FOR vSubPagos IN cur_sub_pagos
    LOOP
        sHistorial :=  sHistorial || 'nombre plan: ' || vSubPagos.NOMBREPLAN || ', fecha de inicio de suscripción: ' || vSubPagos.fechainicio
                        || ', método de pago: ' || vSubPagos.metodopago || ', periodicidad de pago: ' || vSubPagos.tipopago
                        || ', fecha de cancelación: ';
        IF vSubPagos.fechacancelacion is not null THEN
            sHistorial := sHistorial ||  vSubPagos.fechacancelacion;
        ELSE 
            sHistorial := sHistorial || 'SIN DATOS';
        END IF;
        
        IF vSubPagos.fechaefectuado is not null THEN
            sHistorial := sHistorial || ', fecha efectuado: ' || vSubPagos.fechaefectuado;
        ELSE 
            sHistorial := sHistorial || ', fecha efectuado: SIN DATOS';
        END IF;
        
        sHistorial := sHistorial || ', monto pagado: ' || vSubPagos.montoapagar || CHR(13);
        
    END LOOP;
    commit;
    return sHistorial;
END;

CREATE OR REPLACE FUNCTION HISTORIAL_SIN_PAGOS (usuario_subscripcion usuario.usuario%TYPE) RETURN VARCHAR2 AS 
    CURSOR cur_sub_pagos IS
    SELECT nombreplan, fechainicio, fechacancelacion, tipopago, metodopago
    FROM subscripcion
    WHERE subscripcion.usuario = usuario_subscripcion;
    
    
    sHistorial VARCHAR2(30000) := '';
BEGIN 
    FOR vSubPagos IN cur_sub_pagos
    LOOP
        sHistorial := sHistorial || 'nombre plan: ' || vSubPagos.NOMBREPLAN || ', fecha de inicio de suscripción: ' || vSubPagos.fechainicio
                        || ', método de pago: ' || vSubPagos.metodopago || ', periodicidad de pago: ' || vSubPagos.tipopago
                        || ', fecha de cancelación: ';
        IF vSubPagos.fechacancelacion is not null THEN
            sHistorial := sHistorial ||  vSubPagos.fechacancelacion;
        ELSE 
            sHistorial := sHistorial || 'SIN DATOS';
        END IF;
        
        sHistorial := sHistorial || CHR(13);
        
    END LOOP;
    commit;
    return sHistorial;
END;

CREATE OR REPLACE FUNCTION HISTORIAL_SUBSCRIPCIONES_PAGOS (usuario_subscripcion usuario.usuario%TYPE, mostrar_datos_pago factura.estapago%TYPE) RETURN VARCHAR2 AS 
   
    cUsuarios NUMBER := 0;
BEGIN
    SELECT COUNT(1) INTO cUsuarios
    FROM subscripcion s  
    WHERE s.usuario = usuario_subscripcion;
    
    IF (cUsuarios = 0) THEN
        RAISE_APPLICATION_ERROR(-20001, 'No existe el usuario'); 
        commit;
    END IF;

    IF mostrar_datos_pago = 1 THEN
        return HISTORIAL_CON_PAGOS(usuario_subscripcion);
    ELSE
        return HISTORIAL_SIN_PAGOS(usuario_subscripcion);
    END IF;
EXCEPTION
    WHEN OTHERS THEN 
         RAISE_APPLICATION_ERROR(-20003, 'Se produjo un error el cual no fue necesario rollback'); 
END;

--====================================================
--FACTURA
--====================================================
     
CREATE OR REPLACE PROCEDURE PLAN_ACTIVO (estado_plan plan.activado%TYPE, nombre_plan plan.nombre%TYPE) AS
BEGIN
    IF estado_plan = 0 THEN
            UPDATE 
                plan
            SET 
                plan.activado = 1
            WHERE 
                plan.nombre = nombre_plan;
	commit;
    END IF;
EXCEPTION 
    WHEN OTHERS THEN 
        ROLLBACK;
        commit;
        RAISE_APPLICATION_ERROR(-20004, 'Se produjo un error el cual fue necesario rollback');
END;

CREATE OR REPLACE FUNCTION GENERAR_CODIGO_FACTURA RETURN INTEGER AS
    
    cFacturas INTEGER;
    codigoFactura INTEGER;
BEGIN 
    SELECT COUNT(1) INTO cFacturas
    FROM factura;
    
    IF cFacturas = 0 THEN
        commit;
        return 1;
    ELSE 
        SELECT MAX(codigo) INTO codigoFactura
        FROM factura;
        commit;
        return codigoFactura + 1;
    END IF;
EXCEPTION 
    WHEN OTHERS THEN 
       RAISE_APPLICATION_ERROR(-20003, 'Se produjo un error el cual no fue necesario rollback'); 
END;

CREATE OR REPLACE PROCEDURE FACTURACION (fecha_recibida DATE) AS 
    CURSOR cur_subscripcion IS
    SELECT nombreplan, idSubscripcion, usuario, tarifamensual, activado, porcentajedescuento, tipopago, fechacancelacion, fechainicio
    FROM subscripcion, plan
    WHERE subscripcion.nombreplan = plan.nombre;
    
    codigoFactura integer;
BEGIN
    FOR vSubscripcion IN cur_subscripcion
    LOOP
        BEGIN
            IF vSubscripcion.fechacancelacion is null THEN
                PLAN_ACTIVO(vSubscripcion.activado, vSubscripcion.nombreplan);
                codigoFactura := GENERAR_CODIGO_FACTURA;
                IF vSubscripcion.tipopago = 'anual' THEN
                    IF (TO_CHAR(vSubscripcion.fechainicio, 'DD') = TO_CHAR(fecha_recibida, 'DD') AND TO_CHAR(vSubscripcion.fechainicio, 'MM') = TO_CHAR(fecha_recibida, 'MM')) THEN
                        INSERT INTO factura
                        (codigo, idsubscripcion, montoapagar, fechaefectuado, fechaCreada, estapago)
                        VALUES
                        (codigoFactura, vSubscripcion.idSubscripcion, ((vSubscripcion.tarifamensual * 12) * ((100 - vSubscripcion.porcentajedescuento)) / 100), null, fecha_recibida, 0);
                    END IF;
                ELSE
                    IF (TO_CHAR(vSubscripcion.fechainicio, 'DD') = TO_CHAR(fecha_recibida, 'DD')) THEN
                        INSERT INTO factura
                        (codigo, idsubscripcion, montoapagar, fechaefectuado, fechaCreada, estapago)
                        VALUES
                        (codigoFactura, vSubscripcion.idSubscripcion, vSubscripcion.tarifamensual, null, fecha_recibida, 0);
                    END IF;
                END IF;
            END IF;
        commit;
        EXCEPTION 
            WHEN OTHERS THEN 
                ROLLBACK;
                commit;
                RAISE_APPLICATION_ERROR(-20004, 'Se produjo un error el cual fue necesario rollback');
        END;
    END LOOP;
END;    

--====================================================
--Control de pagos
--====================================================


CREATE OR REPLACE PROCEDURE CANCELAR_SUBSCRIPCION(fecha_recibida DATE, id_subscripcion subscripcion.idsubscripcion%TYPE) AS 
BEGIN
    UPDATE 
        subscripcion
    SET 
        subscripcion.fechacancelacion = fecha_recibida
    WHERE 
         subscripcion.idsubscripcion = id_subscripcion;
    commit;
EXCEPTION 
    WHEN OTHERS THEN 
        ROLLBACK;
        commit;
        RAISE_APPLICATION_ERROR(-20004, 'Se produjo un error el cual fue necesario rollback');
END;

CREATE OR REPLACE PROCEDURE CONTROL_PAGOS_TODOS_USUARIOS(fecha_recibida DATE) AS
    CURSOR cur_factura IS 
    SELECT estapago, fechaefectuado, subscripcion.idsubscripcion
    FROM subscripcion, factura
    WHERE subscripcion.idsubscripcion = factura.idsubscripcion;
    
BEGIN
    FOR vFactura IN cur_factura
    LOOP
        IF vFactura.fechaefectuado is not null  THEN 
           IF MONTHS_BETWEEN(fecha_recibida, vFactura.fechaefectuado) > 1  THEN 
                CANCELAR_SUBSCRIPCION(fecha_recibida, vFactura.idSubscripcion);
            END IF;
        END IF;   
    END LOOP;
END;


CREATE OR REPLACE PROCEDURE CONTROL_PAGOS_USUARIO(fecha_recibida DATE, usuario_recibido usuario.usuario%TYPE) AS
    CURSOR cur_factura IS 
    SELECT estapago, fechaefectuado, subscripcion.idsubscripcion
    FROM subscripcion, factura
    WHERE subscripcion.usuario = usuario_recibido 
    AND subscripcion.idsubscripcion = factura.idsubscripcion;
BEGIN
    FOR vFactura IN cur_factura
    LOOP
        IF vFactura.fechaefectuado is not null  THEN 
           IF MONTHS_BETWEEN(fecha_recibida, vFactura.fechaefectuado) > 1  THEN 
                CANCELAR_SUBSCRIPCION(fecha_recibida, vFactura.idSubscripcion);
            END IF;
        END IF;   
    END LOOP;
END;

CREATE OR REPLACE PROCEDURE CONTROL_DE_PAGOS (fecha_recibida DATE, usuario_recibido usuario.usuario%TYPE, todos_los_usuarios factura.estapago%TYPE) AS 
    
BEGIN
    IF todos_los_usuarios = 1 THEN
        CONTROL_PAGOS_TODOS_USUARIOS(fecha_recibida);
    ELSE
        CONTROL_PAGOS_USUARIO(fecha_recibida, usuario_recibido);
    END IF;
EXCEPTION 
    WHEN OTHERS THEN
         RAISE_APPLICATION_ERROR(-20003, 'Se produjo un error el cual no fue necesario rollback');
END; 