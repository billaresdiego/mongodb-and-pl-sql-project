CREATE OR REPLACE TRIGGER USUARIOS_CONTROL_EDAD
BEFORE INSERT OR UPDATE ON usuario
FOR EACH ROW
BEGIN
  	IF (MONTHS_BETWEEN(TRUNC(sysdate), :new.fechanacimiento)/12) < 12 THEN
        	raise_application_error(-20005, 'Los usuarios no pueden ser menores de 12 años');
  	END IF;
END;


CREATE OR REPLACE TRIGGER CONTROL_SUBSCRIPCIONES_USUARIO

--====================================================
--IMPORTANTE: EL SCRIPT DE PRUEBA ENTREGADO CONTIENE MAS DE UNA SUSCRIPCION ACTIVA PARA UN MISMO
--USUARIO. RECOMENDAMOS PROBAR LA FUNCIONALIDAD DE ESTE TRIGGER DESPUES DE CORRER LAS OTRAS PRUEBAS
--====================================================

BEFORE INSERT ON subscripcion
FOR EACH ROW
DECLARE 
    cSubscripcionesUsuario Number := 0;
BEGIN
    SELECT COUNT(1) INTO cSubscripcionesUsuario
    FROM subscripcion
    WHERE subscripcion.usuario = :new.usuario
    AND subscripcion.fechacancelacion is null;
    
  	IF cSubscripcionesUsuario > 0 THEN
        	raise_application_error(-20010, 'No puede haber un usuario con mas de una subscripcion activa');
  	END IF;
END;
