db.datos_basicos.insert({
    "_id" : ObjectId("60d2b21f467e1a308a22ffca"),
    "email" : "Jorge@email.com",
    "nombre" : "Jorge",
    "apellido" : "Tremoloni",
    "fecha_de_nacimiento" : ISODate("1992-04-12T03:00:00.000Z"),
    "preferencias" : {
        "unidades_y_medidas" : "km y kg",
        "temperatura" : "f",
        "deporte" : "ciclismo"
    }
});