db.datos_basicos.insert({
    "_id" : ObjectId("60d2b8f8467e1a308a22ffd1"),
    "email" : "micaelaPilutro@hotmail.com",
    "nombre" : "micaela",
    "apellido" : "Pilutro",
    "genero" : "Femenino",
    "fecha_de_nacimiento" : ISODate("1983-03-09T03:00:00.000Z"),
    "ubicacion" : {
        "calle" : "berbenuch hilorigy",
        "numero" : 5453
    },
    "altura" : 175.096,
    "peso" : 69.2134,
    "biografia" : "Auxiliar de caja",
    "url" : "www.MicaelaVentas.com",
    "preferencias" : {
        "unidades_y_medidas" : "km y kg",
        "temperatura" : "f",
        "deporte" : "ciclismo"
    },
    "controles_de_privacidad" : {
        "pagina_de_perfil" : "todos",
        "actividades_individuales" : "seguidores",
        "actividades_en_grupo" : "todos"
    },
    "equipacion_bicicletas" : [ 
        ObjectId("60d2b66b467e1a308a22ffce")
    ],
    "equipacion_calzado" : [ 
        ObjectId("60d2b67e467e1a308a22ffcf")
    ]
});