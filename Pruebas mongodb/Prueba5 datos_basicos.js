db.datos_basicos.insert({
    "_id" : ObjectId("60d2b3c4467e1a308a22ffcc"),
    "email" : "manuel@email.com",
    "nombre" : "manuel",
    "apellido" : "oknisqui",
    "genero" : "Masculino",
    "fecha_de_nacimiento" : ISODate("1987-02-28T03:00:00.000Z"),
    "controles_de_privacidad" : {
        "pagina_de_perfil" : "seguidores",
        "actividades_individuales" : "todos",
        "actividades_en_grupo" : "seguidores"
    }
});