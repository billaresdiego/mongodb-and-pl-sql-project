db.datos_basicos.insert({
    "_id" : ObjectId("60d2b7c5467e1a308a22ffd0"),
    "email" : "nicolasSansani@hotmail.com",
    "nombre" : "nicolas",
    "apellido" : "sansani",
    "genero" : "Otro",
    "fecha_de_nacimiento" : ISODate("1983-03-09T03:00:00.000Z"),
    "equipacion_bicicletas" : [ 
        ObjectId("60d2b66b467e1a308a22ffce")
    ],
    "equipacion_calzado" : [ 
        ObjectId("60d2b67e467e1a308a22ffcf")
    ]
});